const { ethers } = require('ethers')
require('dotenv').config()

require('@nomiclabs/hardhat-etherscan')
require('@nomiclabs/hardhat-waffle')
require('hardhat-gas-reporter')
require('solidity-coverage')
require('@nomiclabs/hardhat-solhint')
require('@openzeppelin/hardhat-upgrades')
require('hardhat-contract-sizer');

let secrets = require('./.secrets.json')

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task('accounts', 'Prints the list of accounts', async (taskArgs, hre) => {
  const accounts = await ethers.getSigners()

  for (const account of accounts) {
    console.log(account.address)
  }
})

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: {
    compilers: [
      {
        version: '0.8.17',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      },
      {
        version: '0.8.13',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      },
      {
        version: '0.8.11',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      },
      {
        version: "0.8.4",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: "0.8.2",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200
          }
        }
      },
      {
        version: '0.7.5',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      },
      {
        version: '0.5.13',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          }
        },
      }
    ]
  },
  networks: {
    sepolia: {
      url: secrets.sepolia.node_url,
      chainId: 11155111,
      accounts: {
        mnemonic: secrets.sepolia.mnemonic
      },
      gasPrice: 2000000000, // 2Gwei
    },
    chiado: {
      url: secrets.chiado.node_url,
      chainId: 10200,
      accounts: {
        mnemonic: secrets.chiado.mnemonic
      },
      gas: 100000000000, // 100Gwei
      gasPrice: 50000000000, // 50Gwei
    },
    gnosis: {
      url: secrets.gnosis.node_url,
      chainId: 100,
      accounts: secrets.gnosis.pks,
      gasPrice: 1000000000, // 1Gwei
    },
    mainnet: {
      url: secrets.mainnet.node_url,
      chainId: 1,
      accounts: secrets.mainnet.pks,
      gasMultiplier: 1.5
    },
    alfajores: {
      url: secrets.alfajores.node_url,
      chainId: 44787,
      accounts: {
        mnemonic: secrets.alfajores.mnemonic
      }
    },
    celo: {
      url: secrets.celo.node_url,
      chainId: 42220,
      accounts: secrets.celo.pks,
      gasPrice: 10000000000, // 10Gwei
    },
    hardhat: {
      gasLimit: 100000000000, // 100Gwei
      //allowUnlimitedContractSize: true
    }
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== 'false',
    currency: 'USD'
  },
  etherscan: {
    apiKey: {
      mainnet: secrets.mainnet.api_key,
      sepolia: secrets.sepolia.api_key,
      celo: secrets.celo.api_key,
      alfajores: secrets.alfajores.api_key,
      gnosis: secrets.gnosis.api_key,
      chiado: secrets.chiado.api_key
    },
    customChains: [
      {
        network: "alfajores",
        chainId: 44787,
        urls: {
          apiURL: "https://api-alfajores.celoscan.io/api",
          browserURL: "https://alfajores.celoscan.io"
        }
      },
      {
        network: "celo",
        chainId: 42220,
        urls: {
          apiURL: "https://api.celoscan.io/api",
          browserURL: "https://celoscan.io"
        }
      },
      {
        network: "chiado",
        chainId: 10200,
        urls: {
          apiURL: "https://blockscout.com/gnosis/chiado/api",
          browserURL: "https://blockscout.com/gnosis/chiado"
        }
      }
    ]
  },
  mocha: {
    timeout: 100000000
  },
}
