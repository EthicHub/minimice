const { getImplementationAddress } = require('@openzeppelin/upgrades-core')
/* eslint-disable node/no-unsupported-features/es-builtins */
const { ethers, upgrades, network } = require('hardhat')
const DeployResultWriter = require('./utils/deploy-writer')

/*********************************************************************************************************************
 *                                                Migration workflow                                                 *
 *********************************************************************************************************************/
async function deploy(config = {}) {
  const provider = config?.provider ?? config?.deployer?.provider ?? ethers.provider
  const deployer = config?.deployer ?? (await ethers.getSigners())[0]

  const { name, chainId } = await provider.getNetwork()
  console.log(`Network:  ${name} (${chainId})`)
  console.log(`Deployer: ${deployer.address}`)
  console.log(
    `Balance:  ${await provider.getBalance(deployer.address).then(ethers.utils.formatEther)}${
      ethers.constants.EtherSymbol
    }`
  )
  console.log('----------------------------------------------------')

  const writer = new DeployResultWriter(network.name)

  const AccessManager = await ethers.getContractFactory('AccessManager')
  console.log('View upgrader role to deployer...', deployer.address)
  const accessManager = await AccessManager.attach(writer.currentDeployment.accessManager.address)
  const hasRole = await accessManager.hasRole(ethers.utils.id('UPGRADER'), deployer.address)
  if (!hasRole){
    await accessManager.grantRole(ethers.utils.id('UPGRADER'), deployer.address)
    console.log('Set upgrader role to deployer:', deployer.address)
  }
  if (['sepolia', 'alfajores', 'hardhat'].includes(network.name)) {
    const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond')
    console.log('Upgrading ERC20NFTBond...', writer.currentDeployment.miniMiceERC20.address)
    const miniMice = await upgrades.upgradeProxy(
      writer.currentDeployment.miniMiceERC20.address,
      ERC20NFTBond,
      {
        pollingInterval: 100000,
        unsafeAllowRenames: true,
        kind: 'uups'
      }
    );
    const currentImplAddress = await getImplementationAddress(miniMice.provider, miniMice.address)
    console.log(`ERC20NFTBond upgraded: ${miniMice.address}, implementation: ${currentImplAddress}`)
  } else if (['mainnet', 'celo'].includes(network.name)) {
    const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond')
    console.log('Upgrading ERC20NFTBond...', writer.currentDeployment.miniMiceERC20.address)
    const miniMiceAddress = await upgrades.upgradeProxy(
      writer.currentDeployment.miniMiceERC20.address,
      ERC20NFTBond,
      {
        pollingInterval: 1000000,
        unsafeAllowRenames: true,
        kind: 'uups'
      }
    );
    console.log(`ERC20NFTBond prepare upgrade: ${writer.currentDeployment.miniMiceERC20.address}, implementation: ${miniMiceAddress}`)
  }
  if (['gnosis', 'chiado', 'hardhat'].includes(network.name)) {
    const NativeNFTBond = await ethers.getContractFactory('NativeNFTBond')
    console.log('Upgrading NativeNFTBond...', writer.currentDeployment.miniMiceNative.address)
    const miniMice = await upgrades.upgradeProxy(
      writer.currentDeployment.miniMiceNative.address,
      NativeNFTBond,
      {
        pollingInterval: 100000,
        kind: 'uups'
      }
    );
    const currentImplAddress = await getImplementationAddress(miniMice.provider, miniMice.address)
    console.log(`NativeNFTBond upgraded: ${miniMice.address}, implementation: ${currentImplAddress}`)
  }
}

if (require.main === module) {
  deploy({})
    // eslint-disable-next-line no-process-exit
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error)
      // eslint-disable-next-line no-process-exit
      process.exit(1)
    })
}

module.exports = deploy
