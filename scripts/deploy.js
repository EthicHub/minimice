const { getImplementationAddress } = require('@openzeppelin/upgrades-core')
const { BigNumber } = require('ethers')
/* eslint-disable node/no-unsupported-features/es-builtins */
const { ethers, upgrades, network } = require('hardhat')
require("@nomiclabs/hardhat-ethers")
const DeployResultWriter = require('./utils/deploy-writer')

require('dotenv').config()

/*********************************************************************************************************************
 *                                                Migration workflow                                                 *
 *********************************************************************************************************************/
async function deploy(config = {}) {

  const provider = config?.provider ?? config?.deployer?.provider ?? ethers.provider
  const deployer = config?.deployer ?? (await ethers.getSigners())[0]

  const { chainId } = await provider.getNetwork()
  console.log(`Network:  ${network.name} (${chainId})`)
  console.log(`Deployer: ${deployer.address}`)
  console.log(
    `Balance:  ${await provider.getBalance(deployer.address).then(ethers.utils.formatEther)}${
      ethers.constants.EtherSymbol
    }`
  )
  console.log('----------------------------------------------------')

  const contracts = {}
  const writer = new DeployResultWriter(network.name)

  const ERC20Token = await ethers.getContractFactory('ERC20Token')
  if ((!writer.currentDeployment.principalToken.address) && !['gnosis', 'chiado'].includes(network.name)) {
    principal = await ERC20Token.deploy('Principal Token', 'PT')
    await principal.deployed()
    contracts.principal = principal

    console.log(`[0.0] principal token: ${contracts.principal.address}`)
    writer.addContract('principalToken', contracts.principal.address, false)
  } else if (!['gnosis', 'chiado'].includes(network.name)) {
    principal = await ERC20Token.attach(writer.currentDeployment.principalToken.address)
    contracts.principal = principal

    console.log(`[0.0] principal token: ${contracts.principal.address}`)
    writer.addContract('principalToken', contracts.principal.address, false)
  }

  if (!writer.currentDeployment.collateralToken.address) {
    collateral = await ERC20Token.deploy('Collateral Token', 'CT')
    await collateral.deployed()
  } else {
    collateral = await ERC20Token.attach(writer.currentDeployment.collateralToken.address)
  }
  contracts.collateral = collateral

  console.log(`[0.1] collateral token: ${contracts.collateral.address}`)
  writer.addContract('collateralToken', contracts.collateral.address, true)

  const AccessManager = await ethers.getContractFactory('AccessManager')
  if (!writer.currentDeployment.accessManager.address) {
    accessManager = await upgrades.deployProxy(AccessManager, [deployer.address], { unsafeAllow: ['delegatecall'] })
    await accessManager.deployed()
  } else {
    accessManager = await AccessManager.attach(writer.currentDeployment.accessManager.address)
  }
  contracts.accessManager = accessManager

  console.log(`[1] access manager: ${contracts.accessManager.address}`)
  writer.addContract('accessManager', contracts.accessManager.address, true)

  const maturities =  writer.currentDeployment.maturities.map(BigNumber.from)
  const interests = writer.currentDeployment.interests.map(BigNumber.from)
  const nftParams = writer.currentDeployment.nftParams
  const cooldown = BigNumber.from(writer.currentDeployment.cooldown)


  if (['sepolia', 'mainnet', 'alfajores', 'celo', 'hardhat'].includes(network.name)) {
    const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond')
    if (!writer.currentDeployment.miniMiceERC20.address) {
      miniMice = await upgrades.deployProxy(ERC20NFTBond, [
        contracts.principal.address,
        contracts.collateral.address,
        nftParams,
        contracts.accessManager.address,
        interests,
        maturities,
        cooldown
      ])
      await miniMice.deployed()
      if (network.name === 'hardhat') {
        const ERC20NFTBond_v_1_0 = await ethers.getContractFactory('ERC20NFTBond_v_1_0')
        nftParams.baseUri = "ipfs://"
        miniMice_v_1_0 = await upgrades.deployProxy(ERC20NFTBond_v_1_0, [
          contracts.principal.address,
          contracts.collateral.address,
          nftParams,
          contracts.accessManager.address,
          interests,
          maturities
        ])
        await miniMice_v_1_0.deployed()
        contracts.miniMiceERC20_v_1_0 = miniMice_v_1_0
        const ERC20NFTBond_v_1_1 = await ethers.getContractFactory('ERC20NFTBond_v_1_1')
        miniMice_v_1_1 = await upgrades.deployProxy(ERC20NFTBond_v_1_1, [
          contracts.principal.address,
          contracts.collateral.address,
          nftParams,
          contracts.accessManager.address,
          interests,
          maturities
        ])
        await miniMice_v_1_1.deployed()
        contracts.miniMiceERC20_v_1_1 = miniMice_v_1_1
      }
    } else {
      miniMice = await ERC20NFTBond.attach(writer.currentDeployment.miniMiceERC20.address)
    }
    contracts.miniMiceERC20 = miniMice

    const currentImplAddress = await getImplementationAddress(miniMice.provider, contracts.miniMiceERC20.address)
    console.log(`[2] miniMice ERC20: ${contracts.miniMiceERC20.address}, implementation: ${currentImplAddress}`)
    writer.addContract('miniMiceERC20', contracts.miniMiceERC20.address, true)
  }
  if (['gnosis', 'chiado', 'hardhat'].includes(network.name)) {
    const NativeNFTBond = await ethers.getContractFactory('NativeNFTBond')
    if (!writer.currentDeployment.miniMiceNative.address) {
      miniMice = await upgrades.deployProxy(NativeNFTBond, [
        contracts.collateral.address,
        nftParams,
        contracts.accessManager.address,
        interests,
        maturities,
        cooldown
      ])
      await miniMice.deployed()
    } else {
      miniMice = await NativeNFTBond.attach(writer.currentDeployment.miniMiceNative.address)
    }
    contracts.miniMiceNative = miniMice

    const currentImplAddress = await getImplementationAddress(miniMice.provider, contracts.miniMiceNative.address)
    console.log(`[2] miniMice Native: ${contracts.miniMiceNative.address}, implementation: ${currentImplAddress}`)
    writer.addContract('miniMiceNative', contracts.miniMiceNative.address, true)
  }

  // Roles dictionary
  const roles = await Promise.all(
    Object.entries({
      // AccessManager / AccessManaged roles
      DEFAULT_ADMIN: ethers.constants.HashZero,
      INTEREST_PARAMETERS_SETTER: ethers.utils.id('INTEREST_PARAMETERS_SETTER'),
      LIQUIDITY_REQUESTER: ethers.utils.id('LIQUIDITY_REQUESTER'),
      PAUSER: ethers.utils.id('PAUSER'),
      COLLATERAL_BOND_SETTER: ethers.utils.id('COLLATERAL_BOND_SETTER'),
      UPGRADER: ethers.utils.id('UPGRADER'),
      MINTER: ethers.utils.id('MINTER'),
      COOLDOWN_SETTER: ethers.utils.id('COOLDOWN_SETTER'),
    }).map((entry) => Promise.all(entry))
  ).then(Object.fromEntries)

  console.log('[3] roles fetched')

  const ERC20Reserve = await ethers.getContractFactory('ERC20Reserve')
  if (!writer.currentDeployment.collateralReserve.address) {
    collateralReserve = await ERC20Reserve.deploy(contracts.collateral.address)
  } else {
    collateralReserve = await ERC20Reserve.attach(writer.currentDeployment.collateralReserve.address)
  }
  contracts.collateralReserve = collateralReserve

  console.log(`[4] collateral reserve: ${contracts.collateralReserve.address}`)
  writer.addContract('collateralReserve', contracts.collateralReserve.address, false)

  return {
    provider,
    deployer,
    contracts,
    roles,
  }
}

if (require.main === module) {
  deploy({})
    // eslint-disable-next-line no-process-exit
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error)
      // eslint-disable-next-line no-process-exit
      process.exit(1)
    })
}

module.exports = deploy
