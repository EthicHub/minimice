const fs = require('fs')
var path = require('path')
const root = path.resolve('./')
const DIR = path.join(root, '.deployment')
const { BigNumber } = require('ethers')
const beautify = require('json-beautify')
const { getInterestPerSecondFromYearlyInterest } = require('./interest')

const needsInit = (x) => {
  return x === null || x === undefined || x === {}
}

module.exports = class DeployResultWriter {
  constructor(network) {
    this._initDir()
    this.network = network
    this.filePath = path.join(DIR, `${this.network}.json`)
    network === 'hardhat' && fs.existsSync(this.filePath) ? this._removeDeploymentFile() : null
    this._loadDeployment()
  }
  _initDir() {
    if (!fs.existsSync(DIR)) {
      fs.mkdirSync(DIR)
    }
  }
  _getPrincipalToken() {
    const principalToken = this.currentDeployment.principalToken
    return principalToken ? principalToken : {}
  }
  _getCollateralToken() {
    const collateralToken = this.currentDeployment.collateralToken
    return collateralToken ? collateralToken : {}
  }
  _getAccessManager() {
    const accessManager = this.currentDeployment.accessManager
    return accessManager ? accessManager : {}
  }
  _getCollateralReserve() {
    const collateralReserve = this.currentDeployment.collateralReserve
    return collateralReserve ? collateralReserve : {}
  }
  _getNFTParams(){
    return {
      name: 'MiniMice Yield Bond',
      symbol: 'MINIMICE'
    }
  }
  _getInterests(){
    const interests = [
      getInterestPerSecondFromYearlyInterest('6'), // 6% => 190258751903
      getInterestPerSecondFromYearlyInterest('7'), // 7% => 221968543887
      getInterestPerSecondFromYearlyInterest('9'), // 9% => 285388127854
    ].map(String)
    return interests
  }
  _getMaturities(){
    switch (this.network) {
      case 'mainnet':
      case 'gnosis':
      case 'celo':
        var maturities = [
          BigNumber.from('7776000'), // 90 days (aprox. 3 months) -> 6% yearly
          BigNumber.from('15552000'), // 180 days (aprox. 6 months) -> 7% yearly
          BigNumber.from('31536000'), // 365 days -> 9% yearly
        ].map(String)
        return maturities
      case 'sepolia':
      case 'chiado':
      case 'alfajores':
      case 'hardhat':
        var maturities = [
          BigNumber.from('10800'), // 3 hours -> 6% yearly
          BigNumber.from('21600'), // 6 hours -> 7% yearly
          BigNumber.from('43200'), // 12 hours -> 9% yearly
        ].map(String)
        return maturities
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getCooldownParams(){
    switch (this.network) {
      case 'mainnet':
      case 'gnosis':
      case 'celo':
        var cooldown = BigNumber.from('864000') // 10 days
        return cooldown.toString()
      case 'sepolia':
      case 'chiado':
      case 'alfajores':
      case 'hardhat':
        var cooldown = BigNumber.from('3600') // 1 hour
        return cooldown.toString()
      default:
        throw new Error('Unknown or unsupported network: ' + this.network)
    }
  }
  _getMiniMiceERC20() {
    const miniMiceERC20 = this.currentDeployment.miniMiceERC20
    return miniMiceERC20 ? miniMiceERC20 : {}
  }
  _getMiniMiceNative() {
    const miniMiceNative = this.currentDeployment.miniMiceNative
    return miniMiceNative ? miniMiceNative : {}
  }
  _init() {
    needsInit(this.currentDeployment.principalToken) ? (this.currentDeployment.principalToken = this._getPrincipalToken()) : null
    needsInit(this.currentDeployment.collateralToken) ? (this.currentDeployment.collateralToken = this._getCollateralToken()) : null
    needsInit(this.currentDeployment.accessManager) ? (this.currentDeployment.accessManager = this._getAccessManager()) : null
    needsInit(this.currentDeployment.collateralReserve) ? (this.currentDeployment.collateralReserve = this._getCollateralReserve()) : null
    needsInit(this.currentDeployment.interests) ? (this.currentDeployment.interests = this._getInterests()) : null
    needsInit(this.currentDeployment.maturities) ? (this.currentDeployment.maturities = this._getMaturities()) : null
    needsInit(this.currentDeployment.nftParams) ? (this.currentDeployment.nftParams = this._getNFTParams()) : null
    needsInit(this.currentDeployment.cooldown) ? (this.currentDeployment.cooldown = this._getCooldownParams()) : null
    needsInit(this.currentDeployment.miniMiceERC20) ? (this.currentDeployment.miniMiceERC20 = this._getMiniMiceERC20()) : null
    needsInit(this.currentDeployment.miniMiceNative) ? (this.currentDeployment.miniMiceNative = this._getMiniMiceNative()) : null
  }
  _loadDeployment() {
      if (fs.existsSync(this.filePath, fs.R_OK)) {
      this.currentDeployment = JSON.parse(fs.readFileSync(this.filePath))
    } else {
      this.currentDeployment = {}
    }
    this._init()
    this._saveDeploymentToFile()
  }
  _saveDeploymentToFile() {
    fs.writeFileSync(this.filePath, beautify(this.currentDeployment, null, 2, 80))
  }
  _removeDeploymentFile() {
    fs.unlinkSync(this.filePath);
  }
  addContract(name, address, isProxy) {
    if (!this.currentDeployment[name]) {
      this.currentDeployment[name] = {}
    }
    this.currentDeployment[name].address = address
    this.currentDeployment[name].isProxy = isProxy
    this._saveDeploymentToFile()
  }
  updateContract(name, key, value) {
    if (!this.currentDeployment[name]) {
      throw new Error('contract not deployed: '+ name)
    }
    this.currentDeployment[name][key] = value
    this._saveDeploymentToFile()
  }
}
