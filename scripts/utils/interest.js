const { ethers } = require('hardhat')

const YEAR_IN_SECONDS = ethers.BigNumber.from(`${365 * 24 * 60 * 60}`)

function getInterestPerSecondFromYearlyInterest(yearlyInterest) {
  return ethers.utils.parseEther(yearlyInterest).div(YEAR_IN_SECONDS).add(1)
}

module.exports = {
  getInterestPerSecondFromYearlyInterest,
}
