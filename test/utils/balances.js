const { ethers } = require('hardhat')
const { expect } = require('chai')

async function checkBalanceDifferences(signer, action, tokens, from, to, differences) {
  const initialBalancesFrom = await getBalances(from, tokens)
  const initialBalancesTo = await getBalances(to, tokens)
  await signer.sendTransaction(action)
  const finalBalancesFrom = await getBalances(from, tokens)
  const finalBalancesTo = await getBalances(to, tokens)
  for (var i = 0; i < differences.length; i++) {
    expect(finalBalancesTo[i].balance.sub(initialBalancesTo[i].balance)).to.equal(differences[i])
    expect(initialBalancesFrom[i].balance.sub(finalBalancesFrom[i].balance)).to.equal(differences[i])
  }
}

async function getBalances(addresses, tokens) {
  const result = []
  for (const address of addresses) {
    for (const token of tokens) {
      result.push({
        address: address,
        tokenAddress: token.address,
        balance: await token.balanceOf(address),
      })
    }
  }
  return result
}

module.exports = {
  checkBalanceDifferences,
  getBalances,
}
