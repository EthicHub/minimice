const { ethers } = require('hardhat')

const latestTime = async () => {
  const block = await ethers.provider.getBlock('latest')
  return ethers.BigNumber.from(block.timestamp)
}

const increaseTime = async (amount) => {
  ethers.provider.send('evm_increaseTime', [amount])
  ethers.provider.send('evm_mine')
}

module.exports = {
  latestTime,
  increaseTime,
}
