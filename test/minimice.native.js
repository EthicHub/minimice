/*
  MiniMice NativeBond test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub backend platform.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { ethers } = require('hardhat')
const { expect } = require('chai')
const { prepare } = require('./fixture')
const { getInterestPerSecondFromYearlyInterest } = require('../scripts/utils/interest')
const { latestTime, increaseTime } = require('./utils/time')
const { BigNumber } = ethers

describe('Minimice Native', function () {
  prepare()
  let maturities, interests
  before(function () {
    maturities = [
      BigNumber.from('10800'), // 3 hours -> 6% yearly
      BigNumber.from('21600'), // 6 hours -> 7% yearly
      BigNumber.from('43200'), // 12 hours -> 9% yearly
    ]
    interests = [
      getInterestPerSecondFromYearlyInterest('6'),
      getInterestPerSecondFromYearlyInterest('7'),
      getInterestPerSecondFromYearlyInterest('9'),
    ]
  })
  beforeEach(async function () {})

  it('should be properly initialized', async function () {
    expect(await this.contracts.miniMiceNative.name()).to.equal('MiniMice Yield Bond')
    expect(await this.contracts.miniMiceNative.symbol()).to.equal('MINIMICE')

    for (var i = 0; i < maturities.length; i++) {
      expect(await this.contracts.miniMiceNative.maturities(i)).to.equal(maturities[i])
      expect(await this.contracts.miniMiceNative.interests(i)).to.equal(interests[i])
    }
  })

  describe('Buying bonds', function () {
    it('should not allow to buy bond minor than 3 month', async function () {
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0].sub('1'), ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.be.revertedWith(`MaturityInputMustBeGreaterThanFirstMaturity`)
    })
    it('should allow to buy bond for 3 month', async function () {
      const now = await latestTime()
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('1000'))
      expect(await this.contracts.miniMiceNative.tokenURI(0)).to.include('data:application/json;base64')
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer1.address)).to.equal(1)
      expect(await this.contracts.miniMiceNative.ownerOf(0)).to.equal(this.accounts.buyer1.address)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(1)

      const bond = await this.contracts.miniMiceNative.bonds(0)
      expect(bond.mintingDate).to.equal(now.add('1'))
      expect(bond.maturity).to.equal(maturities[0])
      expect(bond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(bond.interest).to.equal(interests[0])
    })
    it('should allow to buy bond for 6 months', async function () {
      const now = await latestTime()
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer2)
          .buyBond(this.accounts.buyer2.address, maturities[1], ethers.utils.parseEther('3000'), 'imageCID', {value: ethers.utils.parseEther('3000')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('3000'))
      expect(await this.contracts.miniMiceNative.tokenURI(1)).to.include('data:application/json;base64')
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer2.address)).to.equal(1)
      expect(await this.contracts.miniMiceNative.ownerOf(1)).to.equal(this.accounts.buyer2.address)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(2)

      const bond = await this.contracts.miniMiceNative.bonds(1)
      expect(bond.mintingDate).to.equal(now.add('1'))
      expect(bond.maturity).to.equal(maturities[1])
      expect(bond.principal).to.equal(ethers.utils.parseEther('3000'))
      expect(bond.interest).to.equal(interests[1])
    })
    it('should allow to buy bond for 12 months', async function () {
      const now = await latestTime()
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer3)
          .buyBond(this.accounts.buyer3.address, maturities[2], ethers.utils.parseEther('5000.333333'), 'imageCID', {value: ethers.utils.parseEther('5000.333333')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('5000.333333'))
      expect(await this.contracts.miniMiceNative.tokenURI(2)).to.include('data:application/json;base64')
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer3.address)).to.equal(1)
      expect(await this.contracts.miniMiceNative.ownerOf(2)).to.equal(this.accounts.buyer3.address)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(3)

      const bond = await this.contracts.miniMiceNative.bonds(2)
      expect(bond.mintingDate).to.equal(now.add('1'))
      expect(bond.maturity).to.equal(maturities[2])
      expect(bond.principal).to.equal(ethers.utils.parseEther('5000.333333'))
      expect(bond.interest).to.equal(interests[2])
    })
    it('should assign max interest if maturity is longer than defined', async function () {
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer4)
          .buyBond(this.accounts.buyer4.address, maturities[2].add('1'), ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('1000'))
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer4.address)).to.equal(1)

      const bond = await this.contracts.miniMiceNative.bonds(3)
      expect(bond.maturity).to.equal(maturities[2].add('1'))
      expect(bond.interest).to.equal(interests[2])
    })
    it('should assign next big interest if maturity is between intervals', async function () {
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer4)
          .buyBond(this.accounts.buyer4.address, maturities[1].add('1'), ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('1000'))
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer4.address)).to.equal(2)

      const bond = await this.contracts.miniMiceNative.bonds(4)
      expect(bond.maturity).to.equal(maturities[1].add('1'))
      expect(bond.interest).to.equal(interests[1])
    })
    it('should allow to buy bond sender != beneficiary', async function () {
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer2.address, maturities[2].add('1'), ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.changeEtherBalance(this.accounts.buyer1, ethers.utils.parseEther('-1000'))
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer2.address)).to.equal(2)
      expect(await this.contracts.miniMiceNative.ownerOf(5)).to.equal(this.accounts.buyer2.address)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(6)

      const bond = await this.contracts.miniMiceNative.bonds(5)
      expect(bond.maturity).to.equal(maturities[2].add('1'))
      expect(bond.interest).to.equal(interests[2])
    })
  })

  describe('NFT', function () {
    it('should allow transfer of bonds, without affecting bond economics', async function () {
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer1.address)).to.equal(1)
      expect(await this.contracts.miniMiceNative.ownerOf(0)).to.equal(this.accounts.buyer1.address)
      const preTxBond = await this.contracts.miniMiceNative.bonds(0)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .transferFrom(this.accounts.buyer1.address, this.accounts.buyer2.address, 0)
      ).to.changeTokenBalance(this.contracts.miniMiceNative, this.accounts.buyer2, 1)
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer1.address)).to.equal(0)
      expect(await this.contracts.miniMiceNative.ownerOf(0)).to.equal(this.accounts.buyer2.address)
      expect(await this.contracts.miniMiceNative.balanceOf(this.accounts.buyer2.address)).to.equal(3)
      const afterTxBond = await this.contracts.miniMiceNative.bonds(0)
      expect(preTxBond).to.deep.equal(afterTxBond)
      // NOTE restore for all the tickets
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer2)
          .transferFrom(this.accounts.buyer2.address, this.accounts.buyer1.address, 0)
      ).to.changeTokenBalance(this.contracts.miniMiceNative, this.accounts.buyer1, 1)
    })
  })

  describe('Borrowing', function () {
    it('should not allow borrowing for not borrower role', async function () {
      expect(await this.contracts.accessManager.hasRole(this.roles.LIQUIDITY_REQUESTER, this.accounts.buyer1.address)).to.be.false
      expect(await this.contracts.accessManager.hasRole(this.roles.LIQUIDITY_REQUESTER, this.accounts.borrower.address)).to.be.true
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .requestLiquidity(this.accounts.borrower.address, ethers.utils.parseEther('1'))
      ).to.be.revertedWith("MissingRole")
    })
    it('should allow borrowing for borrower role', async function () {
      expect(await this.contracts.accessManager.hasRole(this.roles.LIQUIDITY_REQUESTER, this.accounts.borrower.address)).to.be.true
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.borrower)
          .requestLiquidity(this.accounts.borrower.address, ethers.utils.parseEther('1'))
      ).to.changeEtherBalances([this.contracts.miniMiceNative, this.accounts.borrower], [ethers.utils.parseEther('-1'), ethers.utils.parseEther('1')])
    })
    it('should allow returning borrower amount', async function () {
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.borrower)
          .returnLiquidity(ethers.utils.parseEther('1'), {value: ethers.utils.parseEther('1')})
      ).to.changeEtherBalances([this.contracts.miniMiceNative, this.accounts.borrower], [ethers.utils.parseEther('1'), ethers.utils.parseEther('-1')])
    })
  })

  describe('Redeeming bonds', function () {
    it('should fail to redeem bond not in maturity', async function () {
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(0)
      ).to.be.revertedWith(`BondIsLocked`)
    })
    it('should allow to redeem bond', async function () {
      const testBond = await this.contracts.miniMiceNative.bonds(0)

      // We fast-forward maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      const totalSupplyBefore = await this.contracts.miniMiceNative.totalSupply()
      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(0)

      // We fast-forward COOLDOWN
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).redeemBond(0)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(totalSupplyBefore)
    })
    it('should allow to redeem bond sender != beneficiary', async function () {
      const testBond = await this.contracts.miniMiceNative.bonds(1)

      // We fast-forward maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      const totalSupplyBefore = await this.contracts.miniMiceNative.totalSupply()
      await this.contracts.miniMiceNative.connect(this.accounts.buyer2).activateCooldown(1)

      // We fast-forward COOLDOWN
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer2).redeemBond(1)
      expect(await this.contracts.miniMiceNative.totalSupply()).to.equal(totalSupplyBefore)
    })
    it('should fail to redeem bond twice or more', async function () {
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(0)
      ).to.be.revertedWith(`AlreadyRedeemed`)
    })
    it('calculate principal token amount', async function () {
      const testBond = await this.contracts.miniMiceNative.bonds(2)

      // We fast-forward maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer3).activateCooldown(2)

      // We fast-forward COOLDOWN
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).toNumber())

      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, await this.contracts.miniMiceNative.cooldowns(2), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer3)
          .redeemBond(2)
      ).to.changeEtherBalance(this.accounts.buyer3, principalWithInterest)
    })
    it('calculate principal token amount when pass twice of maturity', async function () {
      const testBond = await this.contracts.miniMiceNative.bonds(3)

      // We fast-forward maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer4).activateCooldown(3)

      // We fast-forward twice COOLDOWN
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).mul(2).toNumber())

      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, await this.contracts.miniMiceNative.cooldowns(3), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer4)
          .redeemBond(3)
      ).to.changeEtherBalance(this.accounts.buyer4, principalWithInterest)
    })
  })

  describe('TokenURI bonds', function () {
    it('Decode data uri and compare result', async function () {
      // Get info from tokenId 2
      const tokenId = 2
      const tokenURI = await this.contracts.miniMiceNative.tokenURI(tokenId)
      const bondInfo = await this.contracts.miniMiceNative.bonds(tokenId)
      const principal = ethers.utils.formatEther(bondInfo.principal)
      const maturity = bondInfo.maturity/(60*60*24*30)
      const maturityUnixTimestamp = bondInfo.maturity.add(bondInfo.mintingDate).toString()
      const interest = bondInfo.interest*(60*60*24*365)/1e18
      const collateral = ethers.utils.formatEther((await this.contracts.miniMiceNative.collaterals(tokenId)))
      const ipfsURL = `ipfs://${bondInfo.imageCID}`
      const externalURL = 'https://ethichub.com'

      // decode dataURI
      const encode = tokenURI.replace('data:application/json;base64,', '')
      const decodeJSON = JSON.parse(Buffer.from(encode, 'base64').toString())

      // expect dataURI == token info
      expect(decodeJSON.name).to.equal(`Minimice Yield Bond #${tokenId}`)
      expect(decodeJSON.description).to.equal('MiniMice Risk Yield Bond from EthicHub.')
      expect(decodeJSON.image).to.equal(ipfsURL)
      expect(decodeJSON.external_url).to.equal(externalURL)
      expect(decodeJSON.attributes[0].trait_type).to.equal('Principal')
      expect(decodeJSON.attributes[0].value).to.equal(`${principal.substring(0, decodeJSON.attributes[0].value.length-' USD'.length)} USD`)
      expect(decodeJSON.attributes[1].trait_type).to.equal('Collateral')
      expect(decodeJSON.attributes[1].value).to.equal(`${collateral.substring(0, decodeJSON.attributes[1].value.length-' Ethix'.length)} Ethix`)
      expect(decodeJSON.attributes[2].trait_type).to.equal('APY')
      expect(decodeJSON.attributes[2].value).to.equal(`${interest.toFixed(3).substring(0, decodeJSON.attributes[2].value.length-'%'.length)}%`)
      expect(decodeJSON.attributes[3].trait_type).to.equal('Maturity')
      expect(decodeJSON.attributes[3].value).to.equal(`${maturity.toFixed(3).substring(0, decodeJSON.attributes[3].value.length-' Months'.length)} Months`)
      expect(decodeJSON.attributes[4].trait_type).to.equal('Maturity Unix Timestamp')
      expect(decodeJSON.attributes[4].value).to.equal(maturityUnixTimestamp)
    })
  })

  describe('Interest Parameters', function () {
    it('should allow interest setter to set parameters', async function () {
      maturities = [BigNumber.from('11111'), BigNumber.from('22222'), BigNumber.from('33333')]
      interests = [getInterestPerSecondFromYearlyInterest('11'), getInterestPerSecondFromYearlyInterest('12'), getInterestPerSecondFromYearlyInterest('13')]
      await this.contracts.miniMiceNative
        .connect(this.accounts.interestParametersSetter)
        .setInterestParameters(interests, maturities)
      for (var i; i < maturities.length; i++) {
        expect(await this.contracts.miniMiceNative.maturities(i)).to.equal(maturities[i])
        expect(await this.contracts.miniMiceNative.interests(i)).to.equal(interests[i])
      }
    })
    it('should not allow non interest setter to set parameters', async function () {
      const _maturities = [BigNumber.from('11111'), BigNumber.from('22222')]
      const _interests = [getInterestPerSecondFromYearlyInterest('11'), getInterestPerSecondFromYearlyInterest('12')]
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.other1)
          .setInterestParameters(_interests, _maturities)
      ).to.be.revertedWith('MissingRole')
    })
    it('should not allow unordered maturities', async function () {
      const _maturities = [BigNumber.from('11111999'), BigNumber.from('22222')]
      const _interests = [getInterestPerSecondFromYearlyInterest('11'), getInterestPerSecondFromYearlyInterest('12')]
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.interestParametersSetter)
          .setInterestParameters(_interests, _maturities)
      ).to.be.revertedWith('UnorderedMaturities')
    })
    it('should not allow different size arrays', async function () {
      const _maturities = [BigNumber.from('11111')]
      const _interests = [getInterestPerSecondFromYearlyInterest('11'), BigNumber.from('9')]
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.interestParametersSetter)
          .setInterestParameters(_interests, _maturities)
      ).to.be.revertedWith(`InterestLengthDistinctMaturityLength`)
    })
    it('should not allow to change max parameters', async function () {
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.other1)
          .setMaxInterestParams(10)
      ).to.be.revertedWith('MissingRole')
    })
    it('should allow to change max parameters', async function () {
      await this.contracts.miniMiceNative
        .connect(this.accounts.interestParametersSetter)
        .setMaxInterestParams(4)
      expect(await this.contracts.miniMiceNative.maxParametersLength()).to.equal(4)
    })
  })

  describe('Pause contract', function () {
    it('should not allow pause for non role pauser', async function () {
      expect(await this.contracts.accessManager.hasRole(this.roles.LIQUIDITY_REQUESTER, this.accounts.buyer1.address)).to.be.false
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .pause()
      ).to.be.revertedWith("MissingRole")
    })
    it('if pause, should not buy bonds', async function () {
      await this.contracts.miniMiceNative.connect(this.accounts.pauser).pause()
      expect(await this.contracts.miniMiceNative.paused()).to.true

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})
      ).to.be.revertedWith('Pausable: paused')
    })
    it('if pause, should not request liquidity', async function () {
      expect(await this.contracts.miniMiceNative.paused()).to.true

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.borrower)
          .requestLiquidity(this.accounts.borrower.address, ethers.utils.parseEther('1'))
      ).to.be.revertedWith('Pausable: paused')
    })
    it('if pause, should return liquidity', async function () {
      await this.contracts.miniMiceNative.connect(this.accounts.pauser).unpause()
      expect(await this.contracts.miniMiceNative.paused()).to.false
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.borrower)
          .requestLiquidity(this.accounts.borrower.address, ethers.utils.parseEther('1'))
      ).to.changeEtherBalance(this.accounts.borrower, ethers.utils.parseEther('1'))

      await this.contracts.miniMiceNative.connect(this.accounts.pauser).pause()
      expect(await this.contracts.miniMiceNative.paused()).to.true
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.borrower)
          .returnLiquidity(ethers.utils.parseEther('1'), {value: ethers.utils.parseEther('1')})
      ).to.changeEtherBalance(this.contracts.miniMiceNative, ethers.utils.parseEther('1'))
    })
    it('switch to unpause', async function () {
      await this.contracts.miniMiceNative.connect(this.accounts.pauser).unpause()
      expect(await this.contracts.miniMiceNative.paused()).to.false
    })
  })

  describe('Cooldown manager', function () {
    it('Buyer 1 tries to redeem without activating the cooldown first', async function () {
      await this.contracts.miniMiceNative
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(6)
      ).to.be.revertedWith(`BondIsLocked`)
    })
    it ('Buyer 1 should not active cooldown when not pass maturity', async function () {
      expect(await this.contracts.miniMiceNative.cooldowns(6)).to.be.equal(0)
      await expect(
        this.contracts.miniMiceNative
        .connect(this.accounts.buyer1)
        .activateCooldown(6)
      ).to.be.revertedWith(`CooldownCanNotBeActivated`)
    })
    it ('Buyer 1 should not active cooldown when pass half of maturity', async function () {
      expect(await this.contracts.miniMiceNative.cooldowns(6)).to.be.equal(0)
      const testBond = await this.contracts.miniMiceNative.bonds(6)

      // increase half of maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity.div(2)).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      await expect(
        this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(6)
      ).to.be.revertedWith(`CooldownCanNotBeActivated`)
    })
    it('Buyer 2 should not active cooldown only the owner', async function () {
      expect(await this.contracts.miniMiceNative.cooldowns(6)).to.be.equal(0)
      const testBond = await this.contracts.miniMiceNative.bonds(6)

      // increase maturity
      const now = await latestTime()
      const timeJumpToMaturity = testBond.mintingDate.add(testBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer2)
          .activateCooldown(6)
      ).to.be.revertedWith(`CooldownCanNotBeActivatedNotOwner`)
    })
    it('Buyer 1 unlock the bond, but is not able to redeem before the COOLDOWN passed', async function () {
      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(6)

      // We fast-forward half of COOLDOWN
      const now = await latestTime()
      const timeJumpHalfCooldown = (await this.contracts.miniMiceNative.cooldowns(6)).sub(now).div(2)
      expect((await this.contracts.miniMiceNative.COOLDOWN()).div(2)).to.equal(timeJumpHalfCooldown)
      increaseTime(timeJumpHalfCooldown.toNumber())

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(6)
      ).to.be.revertedWith(`BondIsLocked`)
    })
    it ('Buyer 1 could not active twice cooldown', async function () {
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .activateCooldown(6)
      ).to.be.revertedWith(`CooldownCanNotBeActivated`)
    })
    it('Buyer 1 bond is unlock and should redeems', async function () {
      // We fast-forward COOLDOWN + 1
      const now = await latestTime()
      const timeJumpCooldown = (await this.contracts.miniMiceNative.cooldowns(6)).sub(now).add(1)
      increaseTime(timeJumpCooldown.toNumber())

      const testBond = await this.contracts.miniMiceNative.bonds(6)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceNative.cooldowns(6)), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(6)
      ).to.changeEtherBalance(this.accounts.buyer1, principalWithInterest)
    })
    it('Should not allow to change cooldown to a half because edit another user who is not cooldown setter', async function () {
      const initialCooldown = await this.contracts.miniMiceNative.COOLDOWN()
      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .setCooldown(initialCooldown.div(2))
      ).to.be.revertedWith("MissingRole")
    })
    it('Should allow to change cooldown to a half but redeem not enabled because only pass actual COOLDOWN == 1/2 hour + 1s', async function () {
      await this.contracts.miniMiceNative
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})

      // increase maturity
      increaseTime(maturities[0].toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(7)
      const now = await latestTime()
      expect(await this.contracts.miniMiceNative.cooldowns(7)).to.equal((await this.contracts.miniMiceNative.COOLDOWN()).add(now))

      const initialCooldown = await this.contracts.miniMiceNative.COOLDOWN()
      await this.contracts.miniMiceNative.connect(this.accounts.cooldownSetter).setCooldown(initialCooldown.div(2))
      expect(await this.contracts.miniMiceNative.COOLDOWN()).to.equal(initialCooldown.div(2))

      // We fast-forward half of initial cooldown + 1 second
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).add(1).toNumber())

      await expect(
        this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(7)
      ).to.be.revertedWith(`BondIsLocked`)
    })
    it('Should allow redeem rewards enabled when pass 1 hour', async function () {
      // We fast-forward COOLDOWN + 1
      const now = await latestTime()
      const timeJumpCooldown = (await this.contracts.miniMiceNative.cooldowns(7)).sub(now).add(1)
      increaseTime(timeJumpCooldown.toNumber())

      const testBond = await this.contracts.miniMiceNative.bonds(7)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceNative.cooldowns(7)), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(7)
      ).to.changeEtherBalance(this.accounts.buyer1, principalWithInterest)
    })
    it('Should allow to change cooldown to 3 more times and redeem should be enabled when pass new cooldown time', async function () {
      await this.contracts.miniMiceNative
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})

      // increase maturity
      increaseTime(maturities[0].toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(8)
      const now = await latestTime()
      expect(await this.contracts.miniMiceNative.cooldowns(8)).to.equal((await this.contracts.miniMiceNative.COOLDOWN()).add(now))

      const initialCooldown = await this.contracts.miniMiceNative.COOLDOWN()
      await this.contracts.miniMiceNative.connect(this.accounts.cooldownSetter).setCooldown(initialCooldown.mul(3))
      expect(await this.contracts.miniMiceNative.COOLDOWN()).to.equal(initialCooldown.mul(3))

      // We fast-forward initial cooldown 3 hours
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).toNumber())

      const testBond = await this.contracts.miniMiceNative.bonds(8)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceNative.cooldowns(8)), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer1)
          .redeemBond(8)
      ).to.changeEtherBalance(this.accounts.buyer1, principalWithInterest)
    })
    it('Buyer 2 should not active cooldown only the owner but redeem should be allowed', async function () {
      await this.contracts.miniMiceNative
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID', {value: ethers.utils.parseEther('1000')})

      // increase maturity
      increaseTime(maturities[0].toNumber())

      await this.contracts.miniMiceNative.connect(this.accounts.buyer1).activateCooldown(9)

      // We fast-forward cooldown
      increaseTime((await this.contracts.miniMiceNative.COOLDOWN()).toNumber())

      const testBond = await this.contracts.miniMiceNative.bonds(9)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceNative.cooldowns(9)), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceNative
          .connect(this.accounts.buyer2)
          .redeemBond(9)
      ).to.changeEtherBalance(this.accounts.buyer1, principalWithInterest)
    })
  })

  async function getPrincipalWithInterest(mintingDate, cooldown, interest, principal) {
    // gap of time pass (cooldown - mintingDate)
    const maturityInterestInSeconds = interest.mul(cooldown.sub(mintingDate))
    const BNx18 = BigNumber.from('1000000000000000000')
    return principal.add(principal.mul(maturityInterestInSeconds).div(100).div(BNx18))
  }
})
