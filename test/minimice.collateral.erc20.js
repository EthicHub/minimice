/*
  MiniMice Collateral ERC20 test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub backend platform.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { ethers } = require('hardhat')
const parseEther = ethers.utils.parseEther
const { expect } = require('chai')
const { prepare } = require('./fixture')
const { getInterestPerSecondFromYearlyInterest } = require('../scripts/utils/interest')
const { increaseTime, latestTime } = require('./utils/time')
const { BigNumber } = ethers

describe('Minimice Collateral', function () {
  let haveCollateral = false;
  prepare(haveCollateral)
  let maturities, interests
  before(function () {
    maturities = [
      ethers.BigNumber.from('10800')
    ]
    interests = [
      getInterestPerSecondFromYearlyInterest('6')
    ]
  })
  beforeEach(async function () {})

  it('should be properly initialized', async function () {
    expect(await this.contracts.miniMiceERC20.name()).to.equal('MiniMice Yield Bond')
    expect(await this.contracts.miniMiceERC20.symbol()).to.equal('MINIMICE')
    expect(await this.contracts.miniMiceERC20.principalTokenAddress()).to.equal(this.contracts.principal.address)
    expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
    expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

    for (var i = 0; i < maturities.length; i++) {
      expect(await this.contracts.miniMiceERC20.maturities(i)).to.equal(maturities[i])
      expect(await this.contracts.miniMiceERC20.interests(i)).to.equal(interests[i])
    }
  })

  describe('Collateralization', function () {
    it('should allow to set multiplier for role admin', async function () {
      await this.contracts.miniMiceERC20.connect(this.accounts.collateralBondSetter).setCollateralMultiplier(2)
      expect(await this.contracts.miniMiceERC20.collateralMultiplier()).to.equal(2)
    })
    it('should not allow setting multiplier for non role to set collateral multiplier', async function () {
      await expect(
        this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .setCollateralMultiplier(5)
      ).to.be.revertedWith("MissingRole")
    })
    it('should not allow bond buying if not collateralized', async function () {
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      await expect(
        this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], parseEther('1000'), 'imageCID')
      ).to.be.revertedWith("NotEnoughCollateral")
    })
    it('purchased bonds should have assigned collateral', async function () {
      // all bonds redeemed
      expect(await this.contracts.miniMiceERC20.totalCollateralizedAmount()).to.equal(0)

      // collateral token == 0 and principal token == 0
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      // add collateral token
      collateralMultiplier = await this.contracts.miniMiceERC20.collateralMultiplier()
      principal = parseEther('10')
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, principal.mul(collateralMultiplier))

      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)

      collateral = await this.contracts.miniMiceERC20.collaterals(0)
      expect(collateral).to.equal(principal.mul(collateralMultiplier))
    })
    it('redeeming bond should free collateral', async function () {
      preTotalCollateralizedAmount = await this.contracts.miniMiceERC20.totalCollateralizedAmount()
      collateral = await this.contracts.miniMiceERC20.collaterals(0)

      testBond = await this.contracts.miniMiceERC20.bonds(0)

      await increaseMaturity(testBond.mintingDate, testBond.maturity)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(0)
      increaseTime((await this.contracts.miniMiceERC20.COOLDOWN()).toNumber())

      // add interest of principal token
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceERC20.cooldowns(0)), testBond.interest, testBond.principal)
      await expect(() => this.contracts.principal
          .connect(this.accounts.admin)
          .transfer(this.contracts.miniMiceERC20.address, principalWithInterest.sub(principal))
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principalWithInterest.sub(principal))
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(principalWithInterest)
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(0)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)
      postTotalCollateralizedAmount = await this.contracts.miniMiceERC20.totalCollateralizedAmount()
      expect(postTotalCollateralizedAmount).to.equal(preTotalCollateralizedAmount.sub(collateral))
    })
    it('redeeming bond with zero principal token but has collateral token, should free collateral and transfer collateral', async function () {
      // all bonds redeemed
      expect(await this.contracts.miniMiceERC20.totalCollateralizedAmount()).to.equal(0)
      // has collateral and principal token == 0
      collateralMultiplier = await this.contracts.miniMiceERC20.collateralMultiplier()
      principal = parseEther('10')
      await this.contracts.miniMiceERC20.connect(this.accounts.collateralBondSetter).removeExcessOfCollateral(this.accounts.buyer1.address)
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, principal.mul(collateralMultiplier))
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(principal.mul(collateralMultiplier))
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.borrower)
          .requestLiquidity(this.accounts.borrower.address, principal)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.borrower, principal)
      const testBond = await this.contracts.miniMiceERC20.bonds(1)
      await increaseMaturity(testBond.mintingDate, testBond.maturity)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(1)
      const now = await latestTime()
      expect(await this.contracts.miniMiceERC20.cooldowns(1)).to.equal(now.add(await this.contracts.miniMiceERC20.COOLDOWN()))
      increaseTime((await this.contracts.miniMiceERC20.COOLDOWN()).toNumber())
      // has collateral token not principal token
      collateral = await this.contracts.miniMiceERC20.collaterals(1)
      expect(testBond.principal.mul(collateralMultiplier)).to.equal(collateral)
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      // add interest of collateral token
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceERC20.cooldowns(1)), testBond.interest, testBond.principal)
      await expect(() => this.contracts.collateral
         .connect(this.accounts.admin)
         .transfer(this.contracts.miniMiceERC20.address, principalWithInterest.sub(principal).mul(collateralMultiplier))
      ).to.changeTokenBalance(this.contracts.collateral, this.contracts.miniMiceERC20, principalWithInterest.sub(principal).mul(collateralMultiplier))

      preTotalCollateralizedAmount = await this.contracts.miniMiceERC20.totalCollateralizedAmount()
      expect (testBond.maturity).to.equal(maturities[0])
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(1)
      ).to.changeTokenBalance(this.contracts.collateral, this.accounts.buyer1, principalWithInterest.mul(collateralMultiplier))
      postTotalCollateralizedAmount = await this.contracts.miniMiceERC20.totalCollateralizedAmount()
      expect(postTotalCollateralizedAmount).to.equal(preTotalCollateralizedAmount.sub(collateral))
      expect (testBond.maturity).to.equal(maturities[0])
    })

    it('calculate reedem amount', async function () {
      // all bonds redeemed
      expect(await this.contracts.miniMiceERC20.totalCollateralizedAmount()).to.equal(0)

      // collateral token == 0 and principal token == 0
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      // add collateral token
      collateralMultiplier = await this.contracts.miniMiceERC20.collateralMultiplier()
      principal = parseEther('10')
      // principal * 4 bonds * collateralMultiplier
      totalCollateralizedAmount = principal.mul(collateralMultiplier).mul(4)
      // add collateral token (10 * 4 * collateralMultiplier)
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, totalCollateralizedAmount)

      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)
      await expect(
        () => this.contracts.miniMiceERC20
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)
      await expect(
        () => this.contracts.miniMiceERC20
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)
      await expect(
        () => this.contracts.miniMiceERC20
        .connect(this.accounts.buyer1)
        .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)

      testBond = await this.contracts.miniMiceERC20.bonds(2)
      await increaseMaturity(testBond.mintingDate, testBond.maturity)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(2)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(3)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(4)
      await this.contracts.miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(5)
      increaseTime((await this.contracts.miniMiceERC20.COOLDOWN()).toNumber())

      // 1. with principal
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceERC20.cooldowns(2)), testBond.interest, testBond.principal)
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(2)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)
      // remove excess of collateral
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.collateralBondSetter)
          .removeExcessOfCollateral(this.accounts.buyer1.address)
      ).to.changeTokenBalance(this.contracts.collateral, this.accounts.buyer1, principal.mul(collateralMultiplier))

      // 2. without principal (half of principal so collateral should be half * multiplierColateral)
      // set principal token to 0
      principalBalance =  await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)
      await this.contracts.miniMiceERC20.connect(this.accounts.borrower).requestLiquidity(this.accounts.borrower.address, principalBalance)
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      testBond = await this.contracts.miniMiceERC20.bonds(3)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceERC20.cooldowns(3)), testBond.interest, testBond.principal)
      // add principal token half
      await this.contracts.principal.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, principalWithInterest.div(2))
      // send half * collateral multiplier index of collateral tokens
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(3)
      ).to.changeTokenBalance(this.contracts.collateral,  this.accounts.buyer1, principalWithInterest.div(2).mul(collateralMultiplier))
      // the contract must have zero principal tokens, because it has been delivered to the buyer of the bond
      principalBalance =  await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)
      expect(principalBalance).to.equal(0)
      collateralBalance = await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.collateralBondSetter)
          .removeExcessOfCollateral(this.accounts.buyer1.address)
      ).to.changeTokenBalance(this.contracts.collateral, this.accounts.buyer1, collateralBalance.sub(await this.contracts.miniMiceERC20.totalCollateralizedAmount()))

      // 3. without principal (0) and collateral (2 bonds)
      // collateral token == 2 bonds and principal token == 0
      expect(await this.contracts.miniMiceERC20.totalCollateralizedAmount()).to.equal(principal.mul(collateralMultiplier).mul(2))
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(principal.mul(collateralMultiplier).mul(2))
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)

      // transfer only a 1/4 of principal
      testBond = await this.contracts.miniMiceERC20.bonds(4)
      principalWithInterest = await getPrincipalWithInterest(testBond.mintingDate, (await this.contracts.miniMiceERC20.cooldowns(4)), testBond.interest, testBond.principal)
      await this.contracts.principal.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, principalWithInterest.div(4))
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(4)
      ).to.changeTokenBalance(this.contracts.principal,  this.accounts.buyer1, principalWithInterest.div(4))
      // the contract must have zero principal and collateral tokens, because it has been delivered to the buyer of the bond
      principalBalance =  await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)
      expect(principalBalance).to.equal(0)
      collateralBalance = await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)
      // remove excess of collateral
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.collateralBondSetter)
          .removeExcessOfCollateral(this.accounts.buyer1.address)
      ).to.changeTokenBalance(this.contracts.collateral, this.accounts.buyer1, collateralBalance.sub(await this.contracts.miniMiceERC20.totalCollateralizedAmount()))

      // 4. without principal and with collateral for one bond
      // collateral token only for 1 bond and principal token == 0
      expect(await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(principal.mul(collateralMultiplier))
      expect(await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)).to.equal(0)
      // it should take (principal + interest * collateral), but since there isn't any, take what's left
      collateralBalance = await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)
      expect(collateralBalance).to.equal(principal.mul(collateralMultiplier))
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(5)
      ).to.changeTokenBalance(this.contracts.collateral,  this.accounts.buyer1, collateralBalance)
      principalBalance =  await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)
      expect(principalBalance).to.equal(0)
      // excess == 0
      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.collateralBondSetter)
          .removeExcessOfCollateral(this.accounts.buyer1.address)
      ).to.changeTokenBalance(this.contracts.collateral, this.accounts.buyer1, 0)
      // all bonds redeemed
      expect(await this.contracts.miniMiceERC20.totalCollateralizedAmount()).to.equal(0)
    })
    it('when change the collateral multiplier, the collateral amount is the same', async function () {
      principalBalance =  await this.contracts.principal.balanceOf(this.contracts.miniMiceERC20.address)
      expect(principalBalance).to.equal(0)
      collateralBalance =  await this.contracts.collateral.balanceOf(this.contracts.miniMiceERC20.address)
      expect(collateralBalance).to.equal(0)

      // add collateral token
      collateralMultiplier = await this.contracts.miniMiceERC20.collateralMultiplier()
      principal = parseEther('10')
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.contracts.miniMiceERC20.address, principal.mul(collateralMultiplier))

      await expect(
        () => this.contracts.miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], principal, 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, this.contracts.miniMiceERC20, principal)

      await this.contracts.miniMiceERC20.connect(this.accounts.collateralBondSetter).setCollateralMultiplier(collateralMultiplier.mul(10))
      expect(await this.contracts.miniMiceERC20.collateralMultiplier()).to.equal(collateralMultiplier.mul(10))

      collateral = await this.contracts.miniMiceERC20.collaterals(6)
      expect(collateral).to.equal(principal.mul(collateralMultiplier))
    })
  })

  async function getPrincipalWithInterest(mintingDate, cooldown, interest, principal) {
    // gap of time pass (cooldown - mintingDate)
    const maturityInterestInSeconds = interest.mul(cooldown.sub(mintingDate))
    const BNx18 = BigNumber.from('1000000000000000000')
    return principal.add(principal.mul(maturityInterestInSeconds).div(100).div(BNx18))
  }
  async function increaseMaturity(mintingDate, maturity) {
    const now = await latestTime()
    const timeJumpToMaturity = mintingDate.add(maturity).sub(now)
    increaseTime(timeJumpToMaturity.toNumber())
  }
})
