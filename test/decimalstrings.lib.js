/*
  Decimal to string test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub backend platform.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { expect } = require('chai')

describe('DecimalToString', function () {
  let interestString
  before(async () => {
    const DecimalStringsTest = await ethers.getContractFactory('DecimalStringsTest')
    decimalStringsTest = await DecimalStringsTest.deploy()
    await decimalStringsTest.deployed()
    interestWithZero = 75
    interestWithoutZero = 100075
    interestPercentage = 1023
  })

  it('should change bignumber to string', async function () {
    interestString = await decimalStringsTest.getDecimalString(interestWithZero, 4, false)
    expect(interestString).to.equal('0.0075')
    interestString = await decimalStringsTest.getDecimalString(interestWithoutZero, 4, false)
    expect(interestString).to.equal('10.0075')
    interestString = await decimalStringsTest.getDecimalString(interestPercentage, 2, true)
    expect(interestString).to.equal('10.23%')
  })
})
