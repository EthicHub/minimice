/*
  MiniMice set production parameters test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub backend platform.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
const { ethers } = require('hardhat')
const { expect } = require('chai')
const { prepare } = require('./fixture')
const { getInterestPerSecondFromYearlyInterest } = require('../scripts/utils/interest')


describe('Set Production Parameters', function () {
  prepare()
  let maturities, interests
  before(function () {
    maturities = [
      ethers.BigNumber.from('10800'), // 3 hours -> 6% yearly
      ethers.BigNumber.from('21600'), // 6 hours -> 7% yearly
      ethers.BigNumber.from('43200'), // 12 hours -> 9% yearly
    ]
    interests = [
      getInterestPerSecondFromYearlyInterest('6'),
      getInterestPerSecondFromYearlyInterest('7'),
      getInterestPerSecondFromYearlyInterest('9'),
    ]
  })
  beforeEach(async function () {})
  describe('Interest Parameters', function () {
    it('should be properly initialized', async function () {
      expect(await this.contracts.miniMiceERC20.name()).to.equal('MiniMice Yield Bond')
      expect(await this.contracts.miniMiceERC20.symbol()).to.equal('MINIMICE')
      expect(await this.contracts.miniMiceERC20.principalTokenAddress()).to.equal(this.contracts.principal.address)
  
      for (var i = 0; i < maturities.length; i++) {
        expect(await this.contracts.miniMiceERC20.maturities(i)).to.equal(maturities[i])
        expect(await this.contracts.miniMiceERC20.interests(i)).to.equal(interests[i])
      }
    })

    it('should allow set production parameters', async function () {
      maturities = [
        ethers.BigNumber.from('7776000'), // 90 days (aprox. 3 months) -> 6% yearly
        ethers.BigNumber.from('15552000'), // 180 days (aprox. 6 months) -> 7% yearly
        ethers.BigNumber.from('31536000'), // 365 days -> 9% yearly
      ]
      await this.contracts.miniMiceERC20
        .connect(this.accounts.interestParametersSetter)
        .setInterestParameters(interests, maturities)
      for (var i = 0; i < maturities.length; i++) {
        expect(await this.contracts.miniMiceERC20.maturities(i)).to.equal(maturities[i])
        expect(await this.contracts.miniMiceERC20.interests(i)).to.equal(interests[i])
      }
    })
    it ('calculate maturity and interest exactly production params',async function () {
        const _interests = ['6.00', '7.00', '9.00'];
        const _maturities_days = ['90.00', '180.00', '365.00']; // 90 days (aprox. 3 months), 180 days (aprox. 6 months), 365 days
        for (var i = 0; i < _maturities_days.length; i++) {
          expect((await this.contracts.miniMiceERC20.maturities(i)/(60*60*24)).toFixed(2)).to.equal(_maturities_days[i])
          expect((await this.contracts.miniMiceERC20.interests(i)*(60*60*24*365)/1e18).toFixed(2)).to.equal(_interests[i])
        }
    })
  })
})
