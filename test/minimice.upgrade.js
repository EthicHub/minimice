/*
  MiniMice Upgrade test file.

  Copyright (C) 2022 EthicHub

  This file is part of EthicHub backend platform.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const { ethers } = require('hardhat')
const { expect } = require('chai')
const { prepare } = require('./fixture')
const { getImplementationAddress } = require('@openzeppelin/upgrades-core')
const { getInterestPerSecondFromYearlyInterest } = require('../scripts/utils/interest')
const { latestTime, increaseTime } = require('./utils/time')

const NFT_HASH = '0x0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef'

describe('Minimice', function () {
  prepare()
  let maturities, interests
  before(function () {
    maturities = [
      ethers.BigNumber.from('10800'), // 3 hours -> 6% yearly
      ethers.BigNumber.from('21600'), // 6 hours -> 7% yearly
      ethers.BigNumber.from('43200'), // 12 hours -> 9% yearly
    ]
    interests = [
      getInterestPerSecondFromYearlyInterest('6'),
      getInterestPerSecondFromYearlyInterest('7'),
      getInterestPerSecondFromYearlyInterest('9'),
    ]
  })
  beforeEach(async function () {})
  describe('Upgrade ERC20NFTBond contract', function () {
    it('Another role is not upgrade should not upgrade MiniMice contract', async function () {
      // Upgrade MiniMice
      const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond')
      expect(await ERC20NFTBond.signer.address).to.equal(this.accounts.admin.address)
      await expect(
        upgrades.upgradeProxy(this.contracts.miniMiceERC20.address, ERC20NFTBond)
      ).to.be.revertedWith("MissingRole")
    })
    it('should be upgrader to upgrade MiniMice contract and check params (1.0 -> 1.1)', async function () {
      // old proxy contract instance
      var miniMiceERC20 = this.contracts.miniMiceERC20_v_1_0

      // Check some params
      // collateralMultiplier old version
      expect(await miniMiceERC20.collateralMultiplier()).to.equal(5)
      expect(await miniMiceERC20.collateralTokenAddress()).to.equal(this.contracts.collateral.address)
      // send principal and collateral
      await this.contracts.principal.connect(this.accounts.admin).transfer(miniMiceERC20.address, ethers.utils.parseEther('10'))
      await this.contracts.collateral
        .connect(this.accounts.admin)
        .mint(miniMiceERC20.address, ethers.utils.parseEther('1000000000000000'))
      expect(await this.contracts.principal.balanceOf(miniMiceERC20.address)).to.equal(ethers.utils.parseEther('10'))
      expect(await this.contracts.collateral.balanceOf(miniMiceERC20.address)).to.equal(ethers.utils.parseEther('1000000000000000'))
      await miniMiceERC20.connect(this.accounts.borrower).requestLiquidity(this.accounts.buyer1.address, ethers.utils.parseEther('1'))
      // totalBorrowed old version
      expect(await miniMiceERC20.totalBorrowed()).to.equal(ethers.utils.parseEther('1'))
      // Buy bond
      var rndNonce = Math.floor(Math.random() * 100_000_000_000)
      var signature = await getSignature('ipfsHash', this.accounts.buyer1.address, miniMiceERC20.address, rndNonce, this.accounts.minter)
      // approve principal to buyer1 on NFTBond
      await this.contracts.principal.connect(this.accounts.buyer1).approve(miniMiceERC20.address, ethers.constants.MaxUint256)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond('ipfsHash', this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), NFT_HASH, true, rndNonce, signature)
      ).to.changeTokenBalance(this.contracts.principal, miniMiceERC20, ethers.utils.parseEther('1000'))
      var bond = await miniMiceERC20.bonds(0)
      expect(bond.maturity).to.equal(maturities[0])
      expect(bond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(bond.interest).to.equal(interests[0])
      expect(bond.imageCID).to.be.undefined
      expect(await miniMiceERC20.tokenURI(0)).to.equal('ipfs://ipfsHash')

      // upgrade contract
      const oldImplAddress = await getImplementationAddress(miniMiceERC20.provider, miniMiceERC20.address)
      const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond_v_1_1', this.accounts.upgrader)
      expect(await ERC20NFTBond.signer.address).to.equal(this.accounts.upgrader.address)
      miniMiceERC20 = await upgrades.upgradeProxy(this.contracts.miniMiceERC20_v_1_0.address, ERC20NFTBond)
      const currentImplAddress = await getImplementationAddress(miniMiceERC20.provider, miniMiceERC20.address)
      expect(oldImplAddress).not.to.equal(currentImplAddress)

      // expect is the same proxy
      expect(await miniMiceERC20.name()).to.equal('MiniMice Yield Bond')
      expect(await miniMiceERC20.symbol()).to.equal('MINIMICE')
      for (var i = 0; i < maturities.length; i++) {
        expect(await miniMiceERC20.maturities(i)).to.equal(maturities[i])
        expect(await miniMiceERC20.interests(i)).to.equal(interests[i])
      }
      // Check some params
      // collateralMultiplier and totalBorrowed are the same like old version
      expect(await miniMiceERC20.collateralMultiplier()).to.equal(5)
      expect(await miniMiceERC20.collateralTokenAddress()).to.equal(this.contracts.collateral.address)
      expect(await miniMiceERC20.totalBorrowed()).to.equal(ethers.utils.parseEther('1'))

      // compare new bond with old bond
      expect(bond.maturity).to.equal(maturities[0])
      expect(bond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(bond.interest).to.equal(interests[0])
      expect(bond.imageCID).to.be.undefined
      expect(await miniMiceERC20.tokenURI(0)).to.equal('ipfs://ipfsHash')

      // buy new bond
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, miniMiceERC20, ethers.utils.parseEther('1000'))

      const newBond = await miniMiceERC20.bonds(1)
      expect(newBond.maturity).to.equal(maturities[0])
      expect(newBond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(newBond.interest).to.equal(interests[0])
      expect(newBond.imageCID).to.be.equal('imageCID')
      expect(await miniMiceERC20.tokenURI(1)).to.include('data:application/json;base64')

      // Redeem old bond
      var now = await latestTime()
      var timeJumpToMaturity = bond.mintingDate.add(bond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())
      principalWithInterest = await getPrincipalWithInterestWhenMaturityEnds(bond.maturity, bond.interest, bond.principal)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(0)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)

      // Redeem new bond
      var now = await latestTime()
      var timeJumpToMaturity = newBond.mintingDate.add(newBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())
      principalWithInterest = await getPrincipalWithInterestWhenMaturityEnds(bond.maturity, bond.interest, bond.principal)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(1)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)
    })
    it('should be upgrader to upgrade MiniMice contract and check params (1.1 -> now)', async function () {
      // old proxy contract instance
      var miniMiceERC20 = this.contracts.miniMiceERC20_v_1_1

      // Check some params
      // collateralMultiplier old version
      expect(await miniMiceERC20.collateralMultiplier()).to.equal(5)
      expect(await miniMiceERC20.collateralTokenAddress()).to.equal(this.contracts.collateral.address)
      // send principal and collateral
      await this.contracts.principal.connect(this.accounts.admin).transfer(miniMiceERC20.address, ethers.utils.parseEther('10'))
      await this.contracts.collateral
        .connect(this.accounts.admin)
        .mint(miniMiceERC20.address, ethers.utils.parseEther('1000000000000000'))
      expect(await this.contracts.principal.balanceOf(miniMiceERC20.address)).to.equal(ethers.utils.parseEther('10'))
      expect(await this.contracts.collateral.balanceOf(miniMiceERC20.address)).to.equal(ethers.utils.parseEther('1000000000000000'))
      await miniMiceERC20.connect(this.accounts.borrower).requestLiquidity(this.accounts.buyer1.address, ethers.utils.parseEther('1'))
      // totalBorrowed old version
      expect(await miniMiceERC20.totalBorrowed()).to.equal(ethers.utils.parseEther('1'))
      // approve principal to buyer1 on NFTBond
      await this.contracts.principal.connect(this.accounts.buyer1).approve(miniMiceERC20.address, ethers.constants.MaxUint256)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, miniMiceERC20, ethers.utils.parseEther('1000'))
      var bond = await miniMiceERC20.bonds(0)
      expect(bond.maturity).to.equal(maturities[0])
      expect(bond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(bond.interest).to.equal(interests[0])
      expect(bond.imageCID).to.be.equal('imageCID')
      expect(await miniMiceERC20.tokenURI(0)).to.include('data:application/json;base64')

      // upgrade contract
      const oldImplAddress = await getImplementationAddress(miniMiceERC20.provider, miniMiceERC20.address)
      const ERC20NFTBond = await ethers.getContractFactory('ERC20NFTBond', this.accounts.upgrader)
      expect(await ERC20NFTBond.signer.address).to.equal(this.accounts.upgrader.address)
      // unsafe allow rename variable (NFT:_nonces to NFT:_nonces_deprecated)
      miniMiceERC20 = await upgrades.upgradeProxy(this.contracts.miniMiceERC20_v_1_1.address, ERC20NFTBond, { unsafeAllowRenames: true })
      const currentImplAddress = await getImplementationAddress(miniMiceERC20.provider, miniMiceERC20.address)
      expect(oldImplAddress).not.to.equal(currentImplAddress)

      // expect is the same proxy
      expect(await miniMiceERC20.name()).to.equal('MiniMice Yield Bond')
      expect(await miniMiceERC20.symbol()).to.equal('MINIMICE')
      for (var i = 0; i < maturities.length; i++) {
        expect(await miniMiceERC20.maturities(i)).to.equal(maturities[i])
        expect(await miniMiceERC20.interests(i)).to.equal(interests[i])
      }
      // Check some params
      // collateralMultiplier and totalBorrowed are the same like old version
      expect(await miniMiceERC20.collateralMultiplier()).to.equal(5)
      expect(await miniMiceERC20.collateralTokenAddress()).to.equal(this.contracts.collateral.address)
      expect(await miniMiceERC20.totalBorrowed()).to.equal(ethers.utils.parseEther('1'))
      // check cooldown feature
      expect(await miniMiceERC20.COOLDOWN()).to.equal(0)
      await miniMiceERC20.connect(this.accounts.cooldownSetter).setCooldown(5)
      expect(await miniMiceERC20.COOLDOWN()).to.equal(5)

      // compare new bond with old bond
      expect(bond.maturity).to.equal(maturities[0])
      expect(bond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(bond.interest).to.equal(interests[0])
      expect(bond.imageCID).to.be.equal('imageCID')
      expect(await miniMiceERC20.tokenURI(0)).to.include('data:application/json;base64')

      // buy new bond
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .buyBond(this.accounts.buyer1.address, maturities[0], ethers.utils.parseEther('1000'), 'imageCID')
      ).to.changeTokenBalance(this.contracts.principal, miniMiceERC20, ethers.utils.parseEther('1000'))

      const newBond = await miniMiceERC20.bonds(1)
      expect(newBond.maturity).to.equal(maturities[0])
      expect(newBond.principal).to.equal(ethers.utils.parseEther('1000'))
      expect(newBond.interest).to.equal(interests[0])
      expect(newBond.imageCID).to.be.equal('imageCID')
      expect(await miniMiceERC20.tokenURI(1)).to.include('data:application/json;base64')

      // Redeem old bond
      var now = await latestTime()
      var timeJumpToMaturity = bond.mintingDate.add(bond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())
      await miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(0)
      var cooldown = await miniMiceERC20.COOLDOWN()
      increaseTime(cooldown.toNumber())
      principalWithInterest = await getPrincipalWithInterest(bond.mintingDate, (await miniMiceERC20.cooldowns(0)), bond.interest, bond.principal)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(0)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)

      // Redeem new bond
      var now = await latestTime()
      var timeJumpToMaturity = newBond.mintingDate.add(newBond.maturity).sub(now)
      increaseTime(timeJumpToMaturity.toNumber())
      await miniMiceERC20.connect(this.accounts.buyer1).activateCooldown(1)
      var cooldown = await miniMiceERC20.COOLDOWN()
      increaseTime(cooldown.toNumber())
      principalWithInterest = await getPrincipalWithInterest(newBond.mintingDate, (await miniMiceERC20.cooldowns(1)), newBond.interest, newBond.principal)
      await expect(
        () => miniMiceERC20
          .connect(this.accounts.buyer1)
          .redeemBond(1)
      ).to.changeTokenBalance(this.contracts.principal, this.accounts.buyer1, principalWithInterest)
    })
  })

  async function getPrincipalWithInterestWhenMaturityEnds(maturity, interest, principal) {
    const BNx18 = ethers.BigNumber.from('1000000000000000000')
    return principal.add(principal.mul(interest.mul(maturity)).div(100).div(BNx18))
  }
  async function getPrincipalWithInterest(mintingDate, cooldown, interest, principal) {
    // gap of time pass (cooldown - mintingDate)
    const maturityInterestInSeconds = interest.mul(cooldown.sub(mintingDate))
    const BNx18 = ethers.BigNumber.from('1000000000000000000')
    return principal.add(principal.mul(maturityInterestInSeconds).div(100).div(BNx18))
  }
  async function getSignature(tokenId, buyerAddress, contractAddress, nonce, minter) {
    const network = await ethers.provider.getNetwork()
    const messageHash = ethers.utils.defaultAbiCoder.encode(
      ["string", "address", "bytes32", "address", "bool", "uint256", "uint256"],
      [tokenId, buyerAddress, NFT_HASH, contractAddress, true, nonce, network.chainId]
    )
    const ethSignedMessageHash = ethers.utils.keccak256(messageHash)
    messageHashBytes = ethers.utils.arrayify(ethSignedMessageHash)
    signature = await minter.signMessage(messageHashBytes)
    return signature
  }
})