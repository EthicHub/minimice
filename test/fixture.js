const { ethers } = require('hardhat')
const deploy = require('../scripts/deploy')

var __SNAPSHOT_ID__
function prepare(haveCollateral = true) {
  before(async function () {
    // list signers
    this.accounts = await ethers.getSigners()
    this.accounts.getAccount = (name) => this.accounts[name] || (this.accounts[name] = this.accounts.shift())
    ;[
      'admin',
      'buyer1',
      'buyer2',
      'buyer3',
      'buyer4',
      'borrower',
      'other1',
      'other2',
      'other3',
      'other4',
      'interestParametersSetter',
      'pauser',
      'collateralBondSetter',
      'upgrader',
      'minter',
      'cooldownSetter',
    ].map((name) => this.accounts.getAccount(name))

    // deploy
    await deploy({ force: true, deployer: this.accounts.admin, bondType: 'ALL' }).then((env) => Object.assign(this, env))

    // Set admin as default signer for all contracts
    Object.assign(this, this.contracts)

    // setup roles
    await Promise.all(
      [
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.INTEREST_PARAMETERS_SETTER, this.accounts.interestParametersSetter.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.LIQUIDITY_REQUESTER, this.accounts.borrower.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.PAUSER, this.accounts.pauser.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.COLLATERAL_BOND_SETTER, this.accounts.collateralBondSetter.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.UPGRADER, this.accounts.upgrader.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.MINTER, this.accounts.minter.address),
        this.accessManager
          .connect(this.accounts.admin)
          .grantRole(this.roles.COOLDOWN_SETTER, this.accounts.cooldownSetter.address),
      ].map((txPromise) => txPromise.then((tx) => tx.wait()))
    )
    // Fill token balances
    await Promise.all(
      [
        this.principal
          .connect(this.accounts.admin)
          .mint(this.accounts.buyer1.address, ethers.utils.parseEther('1000000000000000')),
        this.principal
          .connect(this.accounts.admin)
          .mint(this.accounts.buyer2.address, ethers.utils.parseEther('1000000000000000')),
        this.principal
          .connect(this.accounts.admin)
          .mint(this.accounts.buyer3.address, ethers.utils.parseEther('1000000000000000')),
        this.principal
          .connect(this.accounts.admin)
          .mint(this.accounts.buyer4.address, ethers.utils.parseEther('1000000000000000')),
      ].map((txPromise) => txPromise.then((tx) => tx.wait()))
    )

    await Promise.all(
      [
        this.collateral
          .connect(this.accounts.admin)
          .mint(this.accounts.admin.address, ethers.utils.parseEther('1000000000000000')),
      ].map((txPromise) => txPromise.then((tx) => tx.wait()))
    )

    // Approvals
    await this.principal.connect(this.accounts.buyer1).approve(this.miniMiceERC20.address, ethers.constants.MaxUint256)
    await this.principal.connect(this.accounts.buyer2).approve(this.miniMiceERC20.address, ethers.constants.MaxUint256)
    await this.principal.connect(this.accounts.buyer3).approve(this.miniMiceERC20.address, ethers.constants.MaxUint256)
    await this.principal.connect(this.accounts.buyer4).approve(this.miniMiceERC20.address, ethers.constants.MaxUint256)
    await this.principal.connect(this.accounts.borrower).approve(this.miniMiceERC20.address, ethers.constants.MaxUint256)

    if (haveCollateral) {
      // pass 1M of collateral tokens
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.miniMiceERC20.address, ethers.utils.parseEther('1000000'))
      await this.contracts.collateral.connect(this.accounts.admin).transfer(this.miniMiceNative.address, ethers.utils.parseEther('1000000'))
    }

    // eslint-disable-next-line no-undef
    __SNAPSHOT_ID__ = await ethers.provider.send('evm_snapshot')
  })

  after(async function () {
    // eslint-disable-next-line no-undef
    await ethers.provider.send('evm_revert', [__SNAPSHOT_ID__])
  })
}

module.exports = {
  prepare,
}
