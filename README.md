# "Minimice" Yield Bonds (Minimized Risk Yield Bonds)

This repository contains the contracts for the Minimice Yield Bonds.

These bonds will produce different annual yields depending on their maturity and will be collateralized with our token Ethix, minimizing the risk in case of default.

For the first stage of the bonds the yields will be as follows:
| Annual interest | Maturity  |
|:---------------:|:--------: |
| 6%              | 3 months  |
| 7%              | 6 months  |
| 9%              | 12 months |

And the collateral will be x5 Ethix/unit of principal.

Once a bond is purchased, will issue an NFT.

These bonds can be purchased with ERC20 tokens or Native coins.

## Local deployment

### Prerequisite
- Node v14.15.1 or later
- Create `.secrets.json` file in the root following the structure as in `.secrets.template.json`

Clone this repository and run:

``` shell
npm install
```

Make sure all contracts can be compiled:

``` shell
npx hardhat compile
```

Deploy the contract:

``` shell
npx hardhat run --network hardhat scripts/deploy.js
```

To upgrade the contract:

``` shell
npx hardhat run --network hardhat scripts/upgrade.js
```
## Testing

To run the tests:

``` shell
npx hardhat test
```

## License
These contracts are licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).
