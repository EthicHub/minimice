// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.13;

import "./NFTBond.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

/**
 * @title NativeNFTBond
 * @dev Contains functions related to buying and liquidating bonds,
 * and borrowing and returning funds when the principal is native coin
 * @author Ethichub
 */
contract NativeNFTBond is NFTBond {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    IERC20Upgradeable private collateralToken;

    struct NFTParams {
        string name;
        string symbol;
    }

    event TransferDone(address from, address to, uint256 value, bytes data);

    error SendingValueIsDifferentPrincipalInput();
    error FailedToSendNativeCoin();
    error SendingValueIsDifferentAmountInput();

    function initialize(
        address _collateralToken,
        NFTParams calldata _nftParams,
        address _accessManager,
        uint256[] calldata _interests,
        uint256[] calldata _maturities,
        uint256 _cooldownSeconds
    )
    external initializer {
        collateralToken = IERC20Upgradeable(_collateralToken);
        __NFTBond_init(
            _nftParams.name,
            _nftParams.symbol,
            _collateralToken,
            _accessManager,
            _interests,
            _maturities,
            _cooldownSeconds
        );
    }

    /**
     * @dev External function to buy a bond and returns the tokenId of the bond
     * when the contract is active
     * @param beneficiary address
     * @param maturity uint256
     * @param principal uint256
     * @param imageCID string
     */
    function buyBond(
        address beneficiary,
        uint256 maturity,
        uint256 principal,
        string memory imageCID
    )
    external whenNotPaused payable returns (uint256) {
        return super._buyBond(beneficiary, maturity, principal, imageCID);
    }

    /**
     * @dev External function to redeem a bond and returns the amount of the bond
     */
    function redeemBond(uint256 tokenId) external returns (uint256) {
        return super._redeemBond(tokenId);
    }

    function pause() external onlyRole(PAUSER) {
        _pause();
    }

    function unpause() external onlyRole(PAUSER) {
        _unpause();
    }

    receive() external payable {}

    /**
     * @dev Transfers from the buyer to this contract the principal amount
     * @param beneficiary address
     * @param maturity uint256
     * @param principal uint256
     */
    function _beforeBondPurchased(
        address beneficiary,
        uint256 maturity,
        uint256 principal
    )
    internal override {
        super._beforeBondPurchased(beneficiary, maturity, principal);
        if (msg.value != principal) revert SendingValueIsDifferentPrincipalInput();
        emit TransferDone(msg.sender, address(this), principal, bytes(''));
    }

    /**
     * @dev Transfers from the contract to the beneficiary the amount of the bond with its interest.
     * If there is insufficient balance of native coin, will send also the correspondent amount of the collateral
     * @param tokenId uint256
     * @param amount uint256
     * @param beneficiary address
     */
    function _afterBondRedeemed(
        uint256 tokenId,
        uint256 amount,
        address beneficiary
    )
    internal override {
        super._afterBondRedeemed(tokenId, amount, beneficiary);
        if (address(this).balance < amount){
            uint256 amountOfCollateral = (amount - address(this).balance) * collateralMultiplier;
            (bool sent, bytes memory data) = payable(beneficiary).call{value:address(this).balance}('');
            if (! sent) revert FailedToSendNativeCoin();
            emit TransferDone(address(this), beneficiary, address(this).balance, data);
            if (collateralToken.balanceOf(address(this)) < amountOfCollateral) {
                collateralToken.safeTransfer(beneficiary, collateralToken.balanceOf(address(this)));
            } else {
                collateralToken.safeTransfer(beneficiary, amountOfCollateral);
            }
        } else {
            (bool sent, bytes memory data) = payable(beneficiary).call{value:amount}('');
            if (! sent) revert FailedToSendNativeCoin();
            emit TransferDone(address(this), beneficiary, amount, data);
        }
    }

    /**
     * @dev Transfers from the contract to the destination the amount of liquidity for borrowing.
     * @param destination address
     * @param amount uint256
     */
    function _beforeRequestLiquidity(address destination, uint256 amount) internal override {
        (bool sent, bytes memory data) = payable(destination).call{value:amount}('');
        if (! sent) revert FailedToSendNativeCoin();
        emit TransferDone(address(this), destination, amount, data);
        super._beforeRequestLiquidity(destination, amount);
    }

    /**
     * @dev Transfers back to the contract the amount of liquidity used for borrowing.
     * @param amount uint256
     */
    function _afterReturnLiquidity(uint256 amount) internal override {
        super._afterReturnLiquidity(amount);
        if (msg.value != amount) revert SendingValueIsDifferentAmountInput();
        emit TransferDone(msg.sender, address(this), amount, bytes(''));
    }

    function _pause() internal override {
        super._pause();
    }

    function _unpause() internal override {
        super._unpause();
    }

    uint256[49] private __gap;
}