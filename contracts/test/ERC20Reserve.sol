// SPDX-License-Identifier: GPLv3

pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/**
 * @title ERC20Reserve
 * @dev A simple holder of tokens
 * @author Ethichub
 */
contract ERC20Reserve {
    using SafeERC20 for IERC20;
    IERC20 public token;

    event Transfer(address indexed to, uint256 amount);

    constructor(IERC20 _token) {
        token = _token;
    }

    function balance() external view returns (uint256) {
        return token.balanceOf(address(this));
    }

    function transfer(address payable to, uint256 value) external returns (bool) {
        token.safeTransfer(to, value);
        emit Transfer(to, value);
        return true;
    }
}
